﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;

namespace TestScript
{
    class Program
    {

        static void Main(string[] args)
        {
            String logName = "Import_Rejected_ExecutionLog-" + Day() + "-" + Time() + ".txt";
            String fileBackUpName = "_BackUp_" + Day() + "-" + Time() + ".txt";

            String readFile = GetAppConfigValue("readFile");
            StreamWriter swImported = new StreamWriter(GetAppConfigValue("swImported"));
            StreamWriter swRejected = new StreamWriter(GetAppConfigValue("swRejected"));

            StreamWriter swImportedBackUp = new StreamWriter(GetAppConfigValue("swImportedBackUp") + fileBackUpName);
            StreamWriter swRejectedBackUp = new StreamWriter(GetAppConfigValue("swRejectedBackUp") + fileBackUpName);

            StreamWriter swLog = new StreamWriter(GetAppConfigValue("swLog") + logName);

            /** Variable to count the logs steps */
            int logcounter=0;

            WriteIntoFile(swLog, logcounter+". Execution of the script started at: " + Day() + " on: " + Time());
            WriteIntoFile(swLog, "");

            /** Take the current value of the variable logcounter, compute the sum of it's value, and place the result back into the logcounter variable.  */
            logcounter += logcounter;

            /** We try/catch the FileNotFoundException in any case the file is not found in the correct name or path */
            try
            {
                /**
                 * Create an instance of StreamReader to read from a file.
                 */
                using (StreamReader sr = new StreamReader(readFile))
                {

                    String line;

                    String[] splitReadDocLine;
                    String[] splitWriteDocLine;
                    String completeWriteLine;

                    String command;
                    String employeeNumber;
                    String extCOMP;

                    int lineNumber = 0;
                    int imported = 0;
                    int rejectedLineData = 0;
                    int rejectedLineCommand = 0;
                    int rejectedEXTCOM = 0;
                    int rejectedMultipleValues = 0;
                    
                    int commandPosition = 0;
                    int empNoPosition = 0;
                    int extCOMPosition = 0;

                    /** Take the current value of the variable logcounter, compute the sum of it's value, and place the result back into the logcounter variable.  */
                    logcounter += logcounter;

                    while (true)
                    {
                        /**
                         * We add the header of the file we read to the swImported and swRejectted files:
                         ** In the import file we have to add another field, EmployeeID, this field 
                         ** will be filled if it fulfill all the requirements
                         ** The rejected file has the same header
                         */
                        if ((line = sr.ReadLine()) != null && lineNumber == 0)
                        {
                            splitReadDocLine = line.Split(';');
                            splitWriteDocLine = new String[splitReadDocLine.Length + 1];

                            /**
                             * We get the header position value(int), we set this value here to use it for 
                             * search the headers values in every line.
                             */
                            commandPosition = GetHeaderPosition(splitReadDocLine, GetAppConfigValue("command"));
                            empNoPosition = GetHeaderPosition(splitReadDocLine, GetAppConfigValue("empNo"));
                            extCOMPosition = GetHeaderPosition(splitReadDocLine, GetAppConfigValue("extCOM"));

                            /** To read every line it has to be equals to the specific line length = 36 */
                            if (splitReadDocLine.Length == 36)
                            {
                                /** We add the EmployeeID field after the EmployeeNumber on the swImported */
                                for (int i = 0; i < splitWriteDocLine.Length; i++)
                                {
                                    if (i < 5)
                                        splitWriteDocLine[i] = splitReadDocLine[i];
                                    else if (i == 5)
                                        splitWriteDocLine[i] = "EmployeeID";
                                    else if (i > 5)
                                        splitWriteDocLine[i] = splitReadDocLine[i - 1];
                                }
                                /** Once we have filled up the swImported Array we join all the values to write them as a single string */
                                completeWriteLine = string.Join(";", splitWriteDocLine);

                                WriteIntoFile(swImported, completeWriteLine);
                                WriteIntoFile(swRejected, line);

                                WriteIntoFile(swImportedBackUp, completeWriteLine);
                                WriteIntoFile(swRejectedBackUp, line);
                                lineNumber++;
                            }
                            else
                            {
                                WriteIntoFile(swLog, "=============================================================");
                                WriteIntoFile(swLog, (++logcounter) + ". Exception ocurred: The file header mus have 36 data fields, not: " + splitReadDocLine.Length);
                                WriteIntoFile(swLog, "=============================================================");
                                break;
                            }
                        }

                        /**
                         * Read and display lines from the file until the end of the file is reached.
                         */
                        while ((line = sr.ReadLine()) != null && lineNumber > 0)
                        {

                            splitReadDocLine = line.Split(';');

                            splitWriteDocLine = new String[splitReadDocLine.Length + 1];

                            /** To read every line it has to be equals to the specific line length = 36 */
                            if (splitReadDocLine.Length == 36)
                            {
                                /** We get the values of every header according to its position value */
                                command = splitReadDocLine[commandPosition];
                                employeeNumber = splitReadDocLine[empNoPosition];
                                extCOMP = splitReadDocLine[extCOMPosition];

                                /** 
                                 * The begging of the line should start for one of these tres specific command
                                 ** U = Update, A = Add, D = Delete
                                 */
                                if (command.Equals("U") || command.Equals("A") || command.Equals("D"))
                                {
                                    /** DataBase variables to make the queries */
                                    DataTable dtCount = new DataTable();

                                    /** EXTCOMP field is mandaroty so could not be ampty */
                                    if (!extCOMP.Equals(""))
                                    {
                                        String emp_no;
                                        String emp_id;

                                        /** We try/catch a possible TimeOutException in any case we cannot connect to the database */
                                        try
                                        {
                                            dtCount = SqlQueryConsult("SELECT emp_no, emp_id from [asco4].[asco].[employee] where emp_no ='" + employeeNumber + "';");

                                            /** The EmployeeNumber is not on the DDBB --> We add him */
                                            if (dtCount.Rows.Count == 0 && !employeeNumber.Equals("EmployeeNumber") && !employeeNumber.Equals(""))
                                            {
                                                for (int i = 0; i < splitWriteDocLine.Length; i++)
                                                {
                                                    if (i < 5)
                                                        splitWriteDocLine[i] = splitReadDocLine[i];
                                                    else if (i == 5)
                                                        splitWriteDocLine[i] = "";
                                                    else if (i > 5)
                                                        splitWriteDocLine[i] = splitReadDocLine[i - 1];
                                                }
                                                completeWriteLine = string.Join(";", splitWriteDocLine);
                                                WriteIntoFile(swImported, completeWriteLine);
                                                WriteIntoFile(swImportedBackUp, completeWriteLine);
                                                imported++;
                                            }
                                            else if (dtCount != null && dtCount.Rows.Count > 0)
                                            {
                                                foreach (DataRow row in dtCount.Rows)
                                                {
                                                    emp_no = row["emp_no"].ToString();
                                                    emp_id = row["emp_id"].ToString();
                                                    /** First we check that employeeNumber of the line has an equal in the database */
                                                    if (emp_no.Equals(employeeNumber))
                                                    {
                                                        /** More than one result --> We reject the line */
                                                        if (dtCount.Rows.Count > 1)
                                                        {
                                                            WriteIntoFile(swRejected, line);
                                                            WriteIntoFile(swRejectedBackUp, line);
                                                            rejectedMultipleValues++;
                                                            //Console.WriteLine("En la linea: " + lineNumber + " hay mas de un resultado para el EmployeeNumber: " + emp_no + " - Rejected No: " + rejectedMultipleValues);
                                                            break;
                                                        }
                                                        /** One unique result --> We add the employeeID to the line in the IMPORT file */
                                                        else if (dtCount.Rows.Count == 1)
                                                        {
                                                            for (int i = 0; i < splitWriteDocLine.Length; i++)
                                                            {
                                                                if (i < 5)
                                                                    splitWriteDocLine[i] = splitReadDocLine[i];
                                                                else if (i == 5)
                                                                    splitWriteDocLine[i] = emp_id;
                                                                else if (i > 5)
                                                                    splitWriteDocLine[i] = splitReadDocLine[i - 1];
                                                            }
                                                            completeWriteLine = string.Join(";", splitWriteDocLine);
                                                            WriteIntoFile(swImported, completeWriteLine);
                                                            WriteIntoFile(swImportedBackUp, completeWriteLine);
                                                            imported++;
                                                            //Console.WriteLine("En la linea: " + lineNumber + " se añade al documento el EmployeeNumber: " + emp_no + " con EmployeeID: " + emp_id + " - Imported No: " + imported);
                                                        }
                                                        else
                                                        {
                                                            //Console.WriteLine("ERROR");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //Console.WriteLine("ERROR");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //Console.WriteLine("ERROR");
                                            }
                                        }
                                        catch (TimeoutException e)
                                        {
                                            WriteIntoFile(swLog, "=============================================================");
                                            WriteIntoFile(swLog, (++logcounter) + ". Exception ocurred: " + e);
                                            WriteIntoFile(swLog, "=============================================================");
                                        }

                                    }
                                    else
                                    {
                                        WriteIntoFile(swRejected, line);
                                        WriteIntoFile(swRejectedBackUp, line);
                                        rejectedEXTCOM++;
                                        //Console.WriteLine("En la linea: " + lineNumber + " el campo EXT COM no puede estar vacio" + " - EXTCOMRejected No: " + rejectedEXTCOM);
                                    }
                                }
                                else
                                {
                                    WriteIntoFile(swRejected, line);
                                    WriteIntoFile(swRejectedBackUp, line);
                                    rejectedLineCommand++;
                                    //Console.WriteLine("La linea: " + lineNumber + " no tiene un comienzo de linea correcto: " + command);
                                }
                            }
                            else
                            {
                                WriteIntoFile(swRejected, line);
                                WriteIntoFile(swRejectedBackUp, line);
                                rejectedLineData++;
                                //Console.WriteLine("La linea: " + lineNumber + " no tiene 36 campos de datos, tiene: " + splitReadDocLine.Length);
                            }
                            lineNumber++;
                        }
                        int totalRejected = rejectedLineData + rejectedLineCommand + rejectedMultipleValues + rejectedEXTCOM;

                        WriteIntoFile(swLog, "=============================================================");
                        WriteIntoFile(swLog, (++logcounter) + ". Total number of lines scanned on the file: " + lineNumber);
                        WriteIntoFile(swLog, (++logcounter) + ". Total number of lines imported: " + imported);
                        WriteIntoFile(swLog, "=============================================================");
                        WriteIntoFile(swLog, (++logcounter) + ". Total number of lines rejected: " + totalRejected);
                        WriteIntoFile(swLog, "   " + logcounter + ".1 Rejected because the length of the line is not correct: " + rejectedLineData);
                        WriteIntoFile(swLog, "   " + logcounter + ".2 Rejected because the beginning of the line has no correct value (A, D, U): " + rejectedLineCommand);
                        WriteIntoFile(swLog, "   " + logcounter + ".3 Rejected because multiple values on DDBB: " + rejectedMultipleValues);
                        WriteIntoFile(swLog, "   " + logcounter + ".4 Rejected because has no EXTCOM data field: " + rejectedEXTCOM);
                        WriteIntoFile(swLog, "=============================================================");

                        CloseStreamWriter(swImported);
                        CloseStreamWriter(swRejected);

                        CloseStreamWriter(swImportedBackUp);
                        CloseStreamWriter(swRejectedBackUp);
                        break;
                    }                
                }
            }
            catch (FileNotFoundException e)
            {
                WriteIntoFile(swLog, "=============================================================");
                WriteIntoFile(swLog, (++logcounter) + ". Exception ocurred: " + e);
                WriteIntoFile(swLog, "=============================================================");
            }
            WriteIntoFile(swLog, "");
            WriteIntoFile(swLog, (++logcounter) + ". Execution of the script finished at: " + Day() + " on: " + Time());
            CloseStreamWriter(swLog);
        }

        /**
         * Reads from the AppConfig file and gets and return the value of the specific key passed as parameter
         * @parameter: String appConfigKEY, name of the specific KEY to get the value
         * @return: The String value of that specific KEY.
         */
        public static String GetAppConfigValue(String appConfigKEY)
        {
            String value = ConfigurationManager.AppSettings[appConfigKEY];
            return value;
        }

        /**
         * Search the header name into the array and return the position where it is matched
         * @parameter: String[] splitLine, array which we read and compare to the string passed as parameter
         * @parameter: String headerName, string with the desire header field to check passed as parameter
         * @return: The INT position where is the string value in the array
         */
        public static int GetHeaderPosition(String[] splitLine, String headerName)
        {
            for (int i = 0; i < splitLine.Length; i++)
            {
                if (splitLine[i].Equals(headerName))
                    return i;
            }
            return 0;
        }

        public static void CloseStreamWriter(StreamWriter streamWriter)
        {
            streamWriter.Flush();
            streamWriter.Close();
        }

        /**
         * Returns a table in function of the query we pass by parameter
         */
        public static DataTable SqlQueryConsult(String query)
        {

            String connection = GetAppConfigValue("ddbbConexion");
            SqlConnection dbCon = new SqlConnection(connection);
            SqlDataAdapter adaptador = new SqlDataAdapter();
            DataTable ds = new DataTable();

            try
            {
                dbCon.Open();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = query;
                sql.CommandType = CommandType.Text;
                sql.Connection = dbCon;
                adaptador.SelectCommand = sql;
                adaptador.Fill(ds);
                dbCon.Close();
                return ds;
            }
            catch (Exception ex)
            {
                dbCon.Close();
                return null;
            }
        }

        public static void WriteIntoFile(StreamWriter streamWriter, String line)
        {
            streamWriter.WriteLine(line);
        }

        public static String Day()
        {
            String Date = DateTime.Now.ToString("MM.dd.yyyy");
            return Date;
        }

        public static String Time()
        {
            String Time = DateTime.Now.ToString("HH.mm.ss");
            return Time;
        }

    }

}
