#  	SiPass Script Evolution DEV
#  	Creation     : 06/2019 Gonzalo Ponce, Juan Jose Sanchez
#  	Modification :
#  	Description  : 
	This script is designed to read from a SiPass employee
  	data file, check the EmployeeID and EXTCom and check whether the 
  	employee should be in the importation file or the rejection file.

